package com.techtalk.model;

import lombok.Data;

@Data
public class ProductRequest {

    private String categoryName;
}
