package com.techtalk.controller;

import com.techtalk.model.Product;
import com.techtalk.model.ProductRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class ProductController {

    @RequestMapping(value = "/product", method = RequestMethod.POST)
    public ResponseEntity<List<Product>> getAllProductByCategory(@RequestBody ProductRequest productRequest) {
        List<Product> electronics = new ArrayList<>();
        electronics.add(new Product("Sony TV", "Awesome TV", "50K", "2"));
        electronics.add(new Product("Samsung Smartphone",
                "Awesome mobile", "20K", "2"));

        List<Product> homeAppliances = new ArrayList<>();
        homeAppliances.add(new Product("Mixer Grinder", "Awesome mixer", "15K", "2"));


        if (productRequest.getCategoryName() != null
            && productRequest.getCategoryName().equals("electronics")) {
            return ResponseEntity.ok(electronics);

        } else if (productRequest.getCategoryName().equals("homeAppliances")) {
            return ResponseEntity.ok(homeAppliances);
        }
        return ResponseEntity.ok(List.of(new Product()));
    }
}
