# Contract-First Approach Spring Boot Example

## Overview

This project exemplifies the contract-first approach to software development using Spring Boot. The contract-first approach prioritizes the definition of API contracts before the actual implementation, fostering clarity, collaboration, and seamless integration between different services.

## Project Structure

1. **contract-store Module:**
    - Contains the OpenAPI contract YAML files.
    - Acts as the central repository for API definitions.
    - Can be maintained in a separate Git repository for modularity.

2. **dashboard-service Module:**
    - Leverages the contract-store contract by adding it as a dependency.
    - Auto-generates server code using the OpenAPI generator.
    - Illustrates how different services can effortlessly integrate with a shared API contract.

3. **product-service Module:**
    - Represents a standalone service providing API functionality.
    - Can be maintained in a separate Git repository.

## Getting Started

### Prerequisites

- Java JDK 11 or higher
- Maven

### Instructions

1. Clone the repository:

   ```
   git clone https://gitlab.com/techtalk5782345/contract-first-approach-example
   ```

2. Navigate to the `dashboard-service` module:

   ```
   cd dashboard-service
   ```

3. Build the project:

   ```
   mvn clean install
   ```

4. Run the Product Spring Boot application:

   ```
   mvn spring-boot:run
   ```

   #### The Product Service will be accessible at `http://localhost:8080`


5. Run the Dashboard Spring Boot application:

   ```
   mvn spring-boot:run
   ```

   #### The Dashboard Service will be accessible at `http://localhost:8081`.


6. After both the springboot application is up. You can hit the below API in order to test it.

      
      API : http://localhost:8081/getProductsByCategory

      HTTP Method : POST

      Request Body
      
      {
       "categoryName":"electronics"
      }
      
   

## Usage

- The `contract-store` module houses the API contract. Modify the contract YAML files to reflect changes in the API.
- The `dashboard-service` module demonstrates the integration of the contract and auto-generated server code.
- Explore the `product-service` module for a standalone API service.
