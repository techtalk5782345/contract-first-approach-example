package com.techtalk.dashboardservice.controller;

import com.techtalk.api.ProductApi;
import com.techtalk.model.Product;
import com.techtalk.model.ProductRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class DashboardController {

    private ProductApi productApi;

    @RequestMapping(value = "/getProductsByCategory", method = RequestMethod.POST)
    public ResponseEntity<List<Product>> getAllProductByCategory(@RequestBody ProductRequest productRequest) {
        productApi = new ProductApi();
        return productApi.getAllProductByCategoryWithHttpInfo(productRequest);
    }
}
